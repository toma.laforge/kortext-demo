declare namespace Cypress {
  interface Chainable<Subject> {
    /**
     * Take get a cypress object in the Dom
     * Can be a child or parent object
     *
     * @example
     * cy
     *  .take('element')
     *
     */
    take(input: string): Chainable<Subject>;
  }
}
