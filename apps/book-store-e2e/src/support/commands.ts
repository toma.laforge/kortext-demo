// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace

Cypress.Commands.add('take', { prevSubject: 'optional' }, (subject, input) => {
  if (subject) {
    cy.wrap(subject).find(`[data-cy=${input}]`);
  } else {
    cy.get(`[data-cy=${input}]`);
  }
});
