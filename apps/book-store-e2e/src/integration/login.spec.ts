describe('login', () => {
  it('should log successfully', () => {
    cy.intercept(
      {
        method: 'GET',
        url:
          'https://www.googleapis.com/books/v1/volumes?q=flowers+inauthor:keyes&maxResults=1&key=CORRECT_TOKEN',
      },
      {
        statusCode: 200,
      }
    ).as('googleApi');

    cy.visit('/');

    cy.take('token').type('CORRECT_TOKEN');

    cy.take('submit-button').click();
  });

  it('should fail login', () => {
    cy.intercept(
      {
        method: 'GET',
        url:
          'https://www.googleapis.com/books/v1/volumes?q=flowers+inauthor:keyes&maxResults=1&key=CORRECT_TOKEN',
      },
      {
        statusCode: 200,
      }
    ).as('googleApi');

    cy.visit('/');

    cy.take('token').type('WRONG_TOKEN');

    cy.take('submit-button').click();

    cy.take('error').should('be.visible');
  });
});
