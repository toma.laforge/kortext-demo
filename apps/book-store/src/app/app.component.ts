import { Component } from '@angular/core';

@Component({
  selector: 'kortext-root',
  templateUrl: './app.component.html',
})
export class AppComponent {}
