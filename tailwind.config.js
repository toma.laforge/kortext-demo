module.exports = {
  mode: 'jit',
  purge: {
    content: [
      './apps/**/*.{html,ts,css,scss}',
      './libs/**/*.{html,ts,css,scss}',
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
