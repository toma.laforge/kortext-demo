import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GoogleItems } from './state/book.service';

@Injectable()
export class BookShelfApiService {
  constructor(private http: HttpClient) {}

  getBooks(search: string, token: string) {
    return this.http.get<GoogleItems>(
      `https://www.googleapis.com/books/v1/volumes?q=intitle:${search}&maxResults=39&key=${token}`
    );
  }
}
