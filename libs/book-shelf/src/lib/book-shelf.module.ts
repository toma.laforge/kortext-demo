import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { RouterModule } from '@angular/router';
import { UiBookCardModule } from '@kortext/ui/book-card';
import { UiSearchBarModule } from '@kortext/ui/search-bar';
import { UiSortingBarModule } from '@kortext/ui/sorting-bar';
import { BookShelfApiService } from './book-sheld-api.service';
import { BookShelfComponent } from './book-shelf/book-shelf.component';
import { BookService } from './state/book.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: BookShelfComponent,
      },
    ]),
    ScrollingModule,
    MatGridListModule,
    UiBookCardModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
    UiSearchBarModule,
    MatProgressBarModule,
    UiSortingBarModule,
  ],
  declarations: [BookShelfComponent],
  providers: [BookService, BookShelfApiService],
})
export class BookShelfModule {}
