import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Order } from '@datorama/akita';
import { Book } from '@kortext/shared';
import { SelectorValue } from '@kortext/ui/sorting-bar';
import { map } from 'rxjs/operators';
import { BookQuery } from '../state/book.query';
import { BookService } from '../state/book.service';

@Component({
  selector: 'kortext-book-shelf',
  templateUrl: './book-shelf.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookShelfComponent implements OnInit {
  books$ = this.bookQuery.books$;
  loading$ = this.bookQuery.selectLoading();
  error$ = this.bookQuery.selectError();

  sortProperty$ = this.bookQuery.sortProperty$;
  orderAsc$ = this.bookQuery.orderByProperty$.pipe(
    map((order) => order === Order.ASC)
  );

  bookSortingValues: SelectorValue[] = [
    { value: 'title', viewValue: 'title' },
    { value: 'author', viewValue: 'author' },
  ];

  constructor(private bookQuery: BookQuery, private bookService: BookService) {}

  ngOnInit(): void {
    this.bookService.search('test');
  }

  search(value: string) {
    this.bookService.search(value);
  }

  sort(sortProperty: string) {
    this.bookService.updateSort(sortProperty as keyof Book);
  }

  order() {
    this.bookService.updateOrderBy();
  }
}
