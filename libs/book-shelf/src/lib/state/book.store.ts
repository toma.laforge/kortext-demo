import { Injectable } from '@angular/core';
import { EntityState, EntityStore, Order, StoreConfig } from '@datorama/akita';
import { Book } from '@kortext/shared';

export interface BookState extends EntityState<Book, number> {
  sort: keyof Book;
  orderBy: Order;
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'book' })
export class BookStore extends EntityStore<BookState> {
  constructor() {
    super({ sort: 'title', order: Order.ASC });
  }
}
