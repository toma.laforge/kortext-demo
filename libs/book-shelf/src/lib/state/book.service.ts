import { Injectable, OnDestroy } from '@angular/core';
import { Order } from '@datorama/akita';
import { TokenQuery } from '@kortext/login';
import { Book, createBook } from '@kortext/shared';
import { of, Subject } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  finalize,
  first,
  mergeMap,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { BookShelfApiService } from '../book-sheld-api.service';
import { BookStore } from './book.store';

export interface VolumeInfo {
  title: string;
  authors?: string | string[];
  imageLinks?: {
    smallThumbnail: string;
  };
}

export interface GoogleBook {
  id: string;
  volumeInfo: VolumeInfo;
}

export interface GoogleItems {
  items: GoogleBook[];
  kind: string;
  totalItems: number;
}

@Injectable()
export class BookService implements OnDestroy {
  private searchSubject$ = new Subject<string>();

  private readonly destroySubject$ = new Subject<void>();
  readonly destroy$ = this.destroySubject$.asObservable();

  constructor(
    private bookStore: BookStore,
    private bookApi: BookShelfApiService,
    private tokenQuery: TokenQuery
  ) {
    this.initSearch();
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
  }

  private initSearch() {
    this.searchSubject$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        takeUntil(this.destroy$),
        withLatestFrom(this.tokenQuery.token$),
        tap(() => this.bookStore.setLoading(true)),
        tap(() => this.bookStore.setError(null)),
        mergeMap(([search, token]) =>
          this.bookApi.getBooks(search, token).pipe(
            first(),
            tap((items) => this.addToStore(items)),
            catchError((error) => of(this.bookStore.setError({ text: error }))),
            finalize(() => this.bookStore.setLoading(false))
          )
        )
      )
      .subscribe();
  }

  private addToStore(items: GoogleItems) {
    const books = items.items.map((item) =>
      createBook(
        item.id,
        item.volumeInfo.authors
          ? item.volumeInfo.authors instanceof Array
            ? item.volumeInfo.authors[0]
            : item.volumeInfo.authors
          : '',
        item.volumeInfo.title,
        item.volumeInfo.imageLinks?.smallThumbnail
      )
    );
    this.bookStore.set(books);
  }

  search(seachField: string) {
    this.searchSubject$.next(seachField);
  }

  updateSort(sort: keyof Book) {
    this.bookStore.update({ sort });
  }

  updateOrderBy() {
    this.bookStore.update(({ orderBy }) => ({
      orderBy: orderBy === Order.ASC ? Order.DESC : Order.ASC,
    }));
  }
}
