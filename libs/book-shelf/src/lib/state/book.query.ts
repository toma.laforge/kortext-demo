import { Injectable } from '@angular/core';
import { combineQueries, QueryEntity } from '@datorama/akita';
import { switchMap } from 'rxjs/operators';
import { BookState, BookStore } from './book.store';

@Injectable({ providedIn: 'root' })
export class BookQuery extends QueryEntity<BookState> {
  sortProperty$ = this.select('sort');
  orderByProperty$ = this.select('orderBy');
  books$ = combineQueries([this.sortProperty$, this.orderByProperty$]).pipe(
    switchMap(([sortBy, orderBy]) =>
      this.selectAll({ sortBy, sortByOrder: orderBy })
    )
  );

  constructor(protected store: BookStore) {
    super(store);
  }
}
