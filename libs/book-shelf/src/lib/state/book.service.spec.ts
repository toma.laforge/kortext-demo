/* eslint-disable @typescript-eslint/no-explicit-any */
import { TokenQuery } from '@kortext/login';
import { Book } from '@kortext/shared';
import { of, throwError } from 'rxjs';
import { BookShelfApiService } from '../book-sheld-api.service';
import { BookService, GoogleItems } from './book.service';
import { BookStore } from './book.store';

jest.useFakeTimers();

describe('TokenService', () => {
  let fixture: BookService;
  let bookApi: BookShelfApiService;
  let store: BookStore;
  let tokenQuery: TokenQuery;

  beforeEach(() => {
    bookApi = {
      getBooks: jest.fn(),
    } as any;

    store = {
      setLoading: jest.fn(),
      setError: jest.fn(),
      update: jest.fn(),
      set: jest.fn(),
    } as any;

    tokenQuery = {
      token$: of('TOKEN'),
    } as any;

    fixture = new BookService(store, bookApi, tokenQuery);
  });

  afterAll(() => jest.clearAllTimers());

  describe('initSearch', () => {
    it('should search for book', () => {
      const ID = 'ID';
      const AUTHOR = 'Kortext';
      const TITLE = 'title';

      jest.spyOn(bookApi, 'getBooks').mockReturnValue(
        of({
          items: [
            {
              id: ID,
              volumeInfo: {
                title: TITLE,
                authors: AUTHOR,
              },
            },
          ],
        } as GoogleItems)
      );

      const expectedBooks = [
        {
          id: ID,
          author: AUTHOR,
          title: TITLE,
        } as Book,
      ];

      fixture.search('toto');

      jest.runOnlyPendingTimers();

      expect(store.setLoading).toHaveBeenNthCalledWith(1, true);
      expect(store.setLoading).toHaveBeenNthCalledWith(2, false);
      expect(store.setError).toHaveBeenNthCalledWith(1, null);
      expect(bookApi.getBooks).toHaveBeenCalled();

      expect(store.set).toHaveBeenNthCalledWith(1, expectedBooks);
    });

    it('should fail because of serveur error', () => {
      const ERROR = new Error('error');

      jest.spyOn(bookApi, 'getBooks').mockReturnValue(throwError(ERROR));

      fixture.search('toto');

      jest.runOnlyPendingTimers();

      expect(store.setLoading).toHaveBeenNthCalledWith(1, true);
      expect(store.setLoading).toHaveBeenNthCalledWith(2, false);
      expect(store.setError).toHaveBeenNthCalledWith(1, null);
      expect(store.setError).toHaveBeenNthCalledWith(2, { text: ERROR });
      expect(bookApi.getBooks).toHaveBeenCalled();

      expect(store.set).not.toHaveBeenCalled();
    });
  });
});
