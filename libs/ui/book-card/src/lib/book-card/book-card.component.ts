import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Book } from '@kortext/shared';

@Component({
  selector: 'kortext-book-card',
  templateUrl: './book-card.component.html',
  styles: [
    `
      :host {
        @apply w-full h-full;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookCardComponent {
  @Input() book!: Book;
}
