import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { SharedModule } from '@kortext/shared';
import { BookCardComponent } from './book-card/book-card.component';

@NgModule({
  imports: [CommonModule, MatCardModule, SharedModule],
  declarations: [BookCardComponent],
  exports: [BookCardComponent],
})
export class UiBookCardModule {}
