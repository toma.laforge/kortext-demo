import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { SearchBarComponent } from './search-bar/search-bar.component';

@NgModule({
  imports: [CommonModule, MatIconModule],
  declarations: [SearchBarComponent],
  exports: [SearchBarComponent],
})
export class UiSearchBarModule {}
