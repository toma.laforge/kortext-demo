import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'kortext-search-bar',
  templateUrl: './search-bar.component.html',
  styles: [
    `
      mat-icon {
        width: 100%;
        height: 100%;
      }
    `,
  ],
})
export class SearchBarComponent {
  @Output() searchValue = new EventEmitter<string>();

  search(value: string) {
    this.searchValue.emit(value);
  }
}
