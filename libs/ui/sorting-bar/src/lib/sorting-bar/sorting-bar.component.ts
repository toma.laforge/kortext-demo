import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

interface SelectValue {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'kortext-sorting-bar',
  templateUrl: './sorting-bar.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SortingBarComponent {
  @Input() title!: string;
  @Input() values!: SelectValue[];

  @Input() selection!: string;
  @Output() selectionChange = new EventEmitter<string>();

  @Input() orderAsc!: boolean;
  @Output() orderChange = new EventEmitter<string>();

  sort(val: string) {
    this.selectionChange.emit(val);
  }

  changeOrder() {
    this.orderChange.emit();
  }
}
