import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { SortingBarComponent } from './sorting-bar/sorting-bar.component';

@NgModule({
  imports: [CommonModule, MatSelectModule, MatIconModule],
  declarations: [SortingBarComponent],
  exports: [SortingBarComponent],
})
export class UiSortingBarModule {}
