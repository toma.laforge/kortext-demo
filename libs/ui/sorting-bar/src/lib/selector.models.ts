export interface SelectorValue {
  value: string;
  viewValue: string;
}
