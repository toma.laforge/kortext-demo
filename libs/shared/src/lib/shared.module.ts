import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LazyImageDirective } from './directive/lazy-image.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [LazyImageDirective],
  exports: [LazyImageDirective],
})
export class SharedModule {}
