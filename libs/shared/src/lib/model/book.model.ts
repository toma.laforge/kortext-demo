export interface Book {
  id: string;
  author: string;
  title: string;
  image: string;
}

export function createBook(
  id: string,
  author: string,
  title: string,
  image?: string
) {
  return {
    id,
    author,
    title,
    image,
  } as Book;
}
