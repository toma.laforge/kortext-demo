/* eslint-disable @angular-eslint/directive-selector */
import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: 'img'
})
export class LazyImageDirective {
  @Input() set doLazy(condition: boolean | undefined) {
    this.isLazyLoading(condition);
  }

  constructor(private el: ElementRef<HTMLImageElement>) {}

  private isLazyLoading(condition = true) {
    const supports = 'loading' in HTMLImageElement.prototype;

    if (supports && condition) {
      this.el.nativeElement.setAttribute('loading', 'lazy');
    }
  }
}
