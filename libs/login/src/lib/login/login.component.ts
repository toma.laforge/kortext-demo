import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenQuery } from '../state/token.query';
import { TokenService } from '../state/token.service';

@Component({
  selector: 'kortext-login',
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
  loginForm!: FormGroup;
  loading$ = this.tokenQuery.selectLoading();
  error$ = this.tokenQuery.selectError();

  constructor(
    private formBuilder: FormBuilder,
    private tokenQuery: TokenQuery,
    private tokenService: TokenService
  ) {
    this.initForm();
  }

  private initForm(): void {
    this.loginForm = this.formBuilder.group({
      token: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    this.tokenService.tokenValidation(this.loginForm.value.token);
  }
}
