import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface TokenState {
  token: string;
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'token' })
export class TokenStore extends Store<TokenState> {
  constructor() {
    super({ token: '' });
  }
}
