/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { TokenService } from './token.service';
import { TokenStore } from './token.store';

describe('TokenService', () => {
  let fixture: TokenService;
  let http: HttpClient;
  let router: Router;
  let store: TokenStore;

  beforeEach(() => {
    http = {
      get: jest.fn(),
    } as any;

    router = {
      navigate: jest.fn(),
    } as any;

    store = {
      setLoading: jest.fn(),
      update: jest.fn(),
    } as any;

    fixture = new TokenService(store, http, router);
  });

  describe('tokenValidation', () => {
    it('should validate token', async () => {
      const goodToken = 'GOOD_TOKEN';
      jest.spyOn(http, 'get').mockReturnValue(of(''));

      await fixture.tokenValidation(goodToken);

      expect(store.setLoading).toHaveBeenNthCalledWith(1, true);
      expect(store.setLoading).toHaveBeenNthCalledWith(2, false);
      expect(http.get).toHaveBeenCalled();
      expect(store.update).toHaveBeenNthCalledWith(1, { token: goodToken });
      expect(router.navigate).toHaveBeenNthCalledWith(1, ['book-shelf']);
    });
  });
});
