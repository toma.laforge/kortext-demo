import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { TokenStore } from './token.store';

@Injectable({ providedIn: 'root' })
export class TokenService {
  constructor(
    protected store: TokenStore,
    private http: HttpClient,
    private router: Router
  ) {}

  tokenValidation(token: string) {
    this.store.setLoading(true);
    //hack to test API key
    this.http
      .get<void>(
        `https://www.googleapis.com/books/v1/volumes?q=flowers+inauthor:keyes&maxResults=1&key=${token}`
      )
      .pipe(
        first(),
        finalize(() => this.store.setLoading(false))
      )
      .subscribe(
        () => this.validationSuccess(token),
        () => this.store.setError({ text: 'invalid Token' })
      );
  }

  private validationSuccess(token: string) {
    this.store.update({ token });
    this.router.navigate(['book-shelf']);
  }
}
