import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { TokenState, TokenStore } from './token.store';

@Injectable({ providedIn: 'root' })
export class TokenQuery extends Query<TokenState> {
  token$ = this.select('token');

  constructor(protected store: TokenStore) {
    super(store);
  }
}
