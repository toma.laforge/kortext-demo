module.exports = {
  projects: [
    '<rootDir>/apps/book-store',
    '<rootDir>/libs/login',
    '<rootDir>/libs/book-shelf',
    '<rootDir>/libs/shared',
    '<rootDir>/libs/ui/search-bar',
    '<rootDir>/libs/ui/book-card',
    '<rootDir>/libs/ui/sorting-bar',
  ],
};
