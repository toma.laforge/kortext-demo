**How to run/test/lint the application**

```bash
npx nx run-many --all --target=test --parallel
npx nx run-many --all --target=lint --parallel

nx e2e book-store-e2e

nx serve book-store
```

Enjoy!!!
